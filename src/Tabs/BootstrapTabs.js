import $ from 'jquery'
import EventEmitter from 'event-emitter'
import trimStart from 'lodash/trimStart'
import pick from 'lodash/pick'

class BootstrapTabs {
  constructor(target, options = {}) {
    this.$element = $(target)
    options = pick(options, ['bindLocation'])

    this.activeTabKey = null

    this.options = {
      bindLocation: true,
      ...options
    }

    const events = ['show', 'shown', 'hide', 'hidden']
    
    events.forEach(eventName => {
      this.attachCustomTabsEventListener(eventName)
    })

    if (this.options.bindLocation) {
      this.bindLocationListener()
    }
  }

  bindLocationListener() {
    $(window).on('hashchange', () => {
      this.showFromLocation()
    })

    this.on('show', ({ tab }) => {
      window.location.hash = tab.attr('href')
    })
  }

  attachCustomTabsEventListener(eventName) {
    this.$element.on(`${eventName}.bs.tab`, 'a[role=tab]', (evt) => {
      const $tab = $(evt.target)
      // Use key attribute/data if present on the tab element
      const tabId = this.getTabKey($tab)
      const $pane = $($tab.attr('href'))
      const customEvent = { 
        tabId, 
        tab: $tab, 
        pane: $pane, 
        originalEvent: evt, 
        tabKey: tabId,
        preventDefault: () => evt.preventDefault()
      }

      this.emit(eventName, customEvent)
      this.emit(`${eventName}.${tabId}`, customEvent)
    })
  }

  getElement() {
    return this.$element
  }

  getNav() {
    return this.$element.find('[role=tablist],.nav-tabs')
  }

  show(tabKey) {
    let $tab

    if ($tab = this.getTabNav(tabKey)) {
      $tab.tab('show')
    }
  }

  getTabKey($tab) {
    return $tab.data('tab-key') || trimStart($tab.attr('href'), '#')
  }

  getTabNav(tabKey) {
    let self = this

    return this.getTabNavs().filter(function () {
      let $tab = $(this)

      if (self.getTabKey($tab) === tabKey) {
        return true
      }
    }).first()
  }

  getTabNavs() {
    return this.getNav().find('a[role=tab]')
  }

  getTabPane(tabKey) {
    return $(this.getTabNav(tabKey).attr('href'))
  }

  showFromLocation() {
    let { hash } = window.location
    let self = this
    let tabKeyShown = false

    if (hash && hash.length > 0) {
      this.getTabNavs().each(function () {
        if ($(this).attr('href') !== hash) {
          return
        }

        self.show(tabKeyShown = self.getTabKey($(this)))

        return false
      })
    }

    return tabKeyShown
  }
}

EventEmitter(BootstrapTabs.prototype)

export default BootstrapTabs