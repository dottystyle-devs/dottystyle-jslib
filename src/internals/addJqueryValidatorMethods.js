import moment from "moment"

export default function ($) {

  $.validator.addMethod("email", function(value, element) {
    return this.optional( element ) || /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( value );
  }, 'Please enter a valid email address.')

  $.validator.addMethod("before_date", function (value, element, params) {
    const {id, format} = params
    const params_val = $(id).val()
    const d1 = moment(value, format).toDate()
    const d2 = moment(params_val, format).toDate()

    return this.optional(element) || new Date(d1) <= new Date(d2) 

  },'Must be less than end date.')

  $.validator.addMethod("after_date", function (value, element, params) {
    const {id, format} = params
    const params_val = $(id).val()
    const d1 = moment(value, format).toDate()
    const d2 = moment(params_val, format).toDate()

    return this.optional(element) || new Date(d1) >= new Date(d2) 

  },'Must be greater than start date.')


  $.validator.addMethod("valid_date", function(value, element, params) {
    return this.optional(element) || moment(value, params.format).isValid()
  }, (params, element) => params.message || `Please enter a valid date format.`)


  // Add pattern validator, copied from the original source code
  $.validator.addMethod('pattern', function (value, element, param) {
    if (this.optional(element)) {
      return true;
    }

    if (typeof param === "string") {
      param = new RegExp("^(?:" + param + ")$")
    }

    return param.test(value)
  }, 'Invalid format')

}