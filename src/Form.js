import _reduce from 'lodash/reduce' 
import 'jquery-serializejson'
import $ from 'jquery'
import forEach from 'lodash/forEach'

import addJqueryValidatorMethods from './internals/addJqueryValidatorMethods'
import flattenMapToInputValues from './utils/flattenMapToInputValues'

export class SubmissionError {
  constructor(errors) {
    const { _error, ...fieldErrors } = errors
    // Remove general error
    this._generalError = _error
    this.errors = fieldErrors
  }

  hasGeneralError() {
    return typeof this._generalError !== 'undefined'
  }

  getGeneralError() {
    return this._generalError
  }
}

const defaultSerializeValues = (form) => form.serializeJSON()

class Form {
  constructor(params) {
    const { 
      form, rules, submit, onSubmitSuccess, 
      onSubmitFail, serializeValues, reset,
      validateOptions = {}
     } = params
    this.submitHandler = submit
    form.submit(event => event.preventDefault())
      .validate({
        rules,
        onfocusout: function(element) {
          this.element(element)
        },
        submitHandler: this.submit,
        ...validateOptions
      })
    this.form = form
    this.onSubmitSuccess = onSubmitSuccess
    this.onSubmitFail = onSubmitFail
    this.serializeValues = serializeValues || defaultSerializeValues
    this.resetOnSubmit = reset || false

    // Disable native form validation
    this.form.prop('novalidate', true)
  }

  getElement() {
    return this.form
  }

  submit = async () => {
    try {
      this.form.find("input[type=submit]").prop('disabled', true)
      const values = this.serializeValues(this.form, this)
      const result = await this.submitHandler(values, this)
      this.onSubmitSuccess(result, values, this)
      
      if (this.resetOnSubmit) {
        this.reset()
      }
    } catch (err) {
      if (err instanceof SubmissionError) {
        this.handleSubmitFail(err)
        this.showSubmissionErrors(err)

        // show error messages here
        //err.messages 
       
      } else {
        this.handleSubmitFail(err)
      }
    } finally {
      this.form.find("input[type=submit] ").prop("disabled", false)
    }
  }
    
  /**
   * 
   * @param {*} errors 
   */
  showSubmissionErrors(submissionError) {
    // Filter first error message for each key
    const _errors = _reduce(submissionError.errors, (result, messages, key) => {
      return Object.assign(result, { [key]: messages[0] })
    }, {})
    
    this.form.validate().showErrors(_errors)
 
  }

  handleSubmitFail = (err) => {
    if (typeof this.onSubmitFail  === 'function') {
      this.onSubmitFail(err, this)
    } else {
      throw err
    }
    this.form.find("input[type=submit] ").removeAttr("disabled")
  }

  populate(values) {
    let mappedValues = flattenMapToInputValues(values)

    forEach(mappedValues, (value, key) => {
      let $target = this.getElement().find(`:input[name="${key}"]`)

      if ($target.length === 0) {
        return
      }

      switch ($target.attr('type')) {
        case 'radio':
        case 'checkbox':
          $target.filter((key, element) => {
            let $element = $(element)
            let checked = $element.val() !== '' && $element.val().toString() == value

            $element.prop('checked', checked)
          })
          break

        default:
          $target.val(value)
      }
    })

    return this
  }

  reset() {
    this.form[0].reset()
    
    return this
  }
}

export const handleApiResponse = (callback, options={}) => {
  return async (values, form) => {
    const response = await callback(values, form)
    const hasData = typeof response.data === 'object' && response.data

    // Ensure "success" is true, in addition with "ok" flag
    if (response.ok && hasData && response.data.success) {
      return response
    } 
    
    // Handle and display form errors
    if (hasData && (typeof response.data.errors === 'object') && response.status === 422) {
      throw new SubmissionError(response.data.errors)
    } 

    if (hasData && response.data.message) {
      throw new SubmissionError({ _error: response.data.message })
    }

    // Throw a generic error
    const error = new SubmissionError({ _error: 'Ooops! Something went wrong' })
    // Add the response to the submission error
    error.response = response

    throw error
  }
}

addJqueryValidatorMethods($)

export const spinner = $("<i class='fa fa-circle-o-notch fa-spin'></i> Loading")

export const enable = () => (form) => {
  form.getElement().find(':submit').each(function () {
    let $button = $(this)
    $button.prop('disabled', false)

    // Revert to old button content
    const originalContent = $button.data('original-innerHtml')

    if (originalContent) {
      $button.empty().html(originalContent)
    }
  })
}

export const disable = ($loader = spinner) => (form) => {
  form.getElement().find(':submit').each(function () {
    let $button = $(this)
    $button.prop('disabled', true)
    
    if ($loader) {
      const originalContent = $button.html()
      $button.data('original-innerHtml', originalContent)
      $button.empty().append($loader)
    }
  })
}

export default Form