import $ from 'jquery'
import randomString from 'randomstring'

const generateRandomString = (options={}) => randomString.generate(options)

class PasswordGenerator {
  constructor($element, options) {
    this.$element = $element
    this.randomOptions = options

    this.$element.on('click', '.generate', (evt) => {
      this.generate()
      evt.preventDefault()
    })
  }

  generate() {
    let $password = this.$element.find(':input.password')
    let password = generateRandomString(this.randomOptions)
    $password.val(password)
    this.$element.trigger('password:generated', [ password, $password ])
  }
}

class PasswordGeneratorExtension {
  constructor(options) {
    this.options = {
      length: 12,
      ...options
    }
  }

  name() {
    return 'Password Generator'
  }

  install(app) {
    app.passwordGenerator = this.createPasswordGenerator
  }

  createPasswordGenerator = (selector, options = {}) => {
    return new PasswordGenerator($(selector), { ...this.options, ...options })
  }
}

export default PasswordGeneratorExtension