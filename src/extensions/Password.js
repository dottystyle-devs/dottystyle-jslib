import $ from 'jquery'

class PasswordWithToggle {
  constructor($element, options) {
    this.$element = $element
    this.masked = true
    this.maskedClass = options.maskedClass
    this.unmaskedClass = options.unmaskedClass

    this.$element.on('click', '.toggle-mask', this.click.bind(this))

    this.mask()
  }

  click(evt) {
    this.masked ? this.unmask() : this.mask()
    evt.preventDefault()
  }

  passwordElement() {
    return this.$element.find(':input.password')
  }

  toggleElement() {
    return this.$element.find('.toggle-mask')
  }

  mask() {
    this.passwordElement().attr('type', 'password')
    this.toggleElement().removeClass(this.unmaskedClass).addClass(this.maskedClass)
    this.masked = true
  }

  unmask() {
    this.passwordElement().attr('type', 'text')
    this.toggleElement().removeClass(this.maskedClass).addClass(this.unmaskedClass)
    this.masked = false
  }
}

class Password {
  name() {
    return 'Password'
  }

  install(app) {
    app.password = this.createPassword
  }

  createPassword = (selector, options) => new PasswordWithToggle($(selector).closest('.password-wrapper'), options)
}

export default Password