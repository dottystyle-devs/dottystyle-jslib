import forEach from 'lodash/forEach'

const flattenMapToInputValues = (value, key = '', result = {}) => {
  if ((value instanceof Array) || 
    (typeof value === 'object' && value && Object.keys(value).length > 0)
  ) {
    forEach(value, (subValue, subKey) => 
      flattenMapToInputValues(subValue, key ? `${key}[${subKey}]` : subKey, result)
    )
  } else if (key) {
    result[key] = value
  }

  return result
}

export default flattenMapToInputValues