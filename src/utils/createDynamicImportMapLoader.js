const onCreateInstanceDefault = (instance) => instance

/**
  * Create a class loader using dynamic imports in a map/object.
  * 
  * @param {Object} importers
  * @param {Function} [onCreateInstance]
  * @return {Function}
  */
const createDynamicImportMapLoader = (importers, onCreateInstance=onCreateInstanceDefault) => {
  return async (name, ...args) => {
    if (typeof importers[name] !== 'undefined') {
      const Module = await importers[name]()
      const instance = new Module.default(...args)

      return onCreateInstance(instance, name, ...args)
    }
  }
}

export default createDynamicImportMapLoader