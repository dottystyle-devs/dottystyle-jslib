import Route from 'route-parser'

/**
 * 
 * @return {String}
 */
const route = (pattern, routeParams=null) => {
  const _route = new Route(pattern)

  return _route.reverse(routeParams)
}

export default route