import normalizeUrl from 'normalize-url'
import trimStart from 'lodash/trimStart'

/**
 * 
 * @param {Function} callback
 * @return {Function}  
 */
const createCallbackGenerator = (callback) => {
  let baseUrl

  return (path='') => {
    baseUrl || (baseUrl = normalizeUrl(callback()))

    return getPath(baseUrl, path)
  }
}

/**
 * Generate a url given the base url and path
 * 
 * @param {String} baseUrl
 * @param {String} path
 * @return {String} 
 */
const getPath = (baseUrl, path='') => {
  // Remove leading slash
  path = trimStart(path, '/')
  
  return path.length > 0 ? `${baseUrl}/${path}` : baseUrl
}

/**
 * 
 * @param {String|Function} baseUrl
 * @return {Function} 
 */
const createUrlWithBaseGenerator = (baseUrl) => {
  if (typeof baseUrl === 'function') {
    return createCallbackGenerator(baseUrl)
  } 
    
  const normalizedBaseUrl = normalizeUrl(baseUrl)

  return (path='') => getPath(normalizedBaseUrl, path)
}

export default createUrlWithBaseGenerator
