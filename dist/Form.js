'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.disable = exports.enable = exports.spinner = exports.handleApiResponse = exports.SubmissionError = undefined;

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _reduce2 = require('lodash/reduce');

var _reduce3 = _interopRequireDefault(_reduce2);

require('jquery-serializejson');

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _forEach = require('lodash/forEach');

var _forEach2 = _interopRequireDefault(_forEach);

var _addJqueryValidatorMethods = require('./internals/addJqueryValidatorMethods');

var _addJqueryValidatorMethods2 = _interopRequireDefault(_addJqueryValidatorMethods);

var _flattenMapToInputValues = require('./utils/flattenMapToInputValues');

var _flattenMapToInputValues2 = _interopRequireDefault(_flattenMapToInputValues);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SubmissionError = exports.SubmissionError = function () {
  function SubmissionError(errors) {
    (0, _classCallCheck3.default)(this, SubmissionError);
    var _error = errors._error,
        fieldErrors = (0, _objectWithoutProperties3.default)(errors, ['_error']);
    // Remove general error

    this._generalError = _error;
    this.errors = fieldErrors;
  }

  (0, _createClass3.default)(SubmissionError, [{
    key: 'hasGeneralError',
    value: function hasGeneralError() {
      return typeof this._generalError !== 'undefined';
    }
  }, {
    key: 'getGeneralError',
    value: function getGeneralError() {
      return this._generalError;
    }
  }]);
  return SubmissionError;
}();

var defaultSerializeValues = function defaultSerializeValues(form) {
  return form.serializeJSON();
};

var Form = function () {
  function Form(params) {
    var _this = this;

    (0, _classCallCheck3.default)(this, Form);
    this.submit = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var values, result;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;

              _this.form.find("input[type=submit]").prop('disabled', true);
              values = _this.serializeValues(_this.form, _this);
              _context.next = 5;
              return _this.submitHandler(values, _this);

            case 5:
              result = _context.sent;

              _this.onSubmitSuccess(result, values, _this);

              if (_this.resetOnSubmit) {
                _this.reset();
              }
              _context.next = 13;
              break;

            case 10:
              _context.prev = 10;
              _context.t0 = _context['catch'](0);

              if (_context.t0 instanceof SubmissionError) {
                _this.handleSubmitFail(_context.t0);
                _this.showSubmissionErrors(_context.t0);

                // show error messages here
                //err.messages 
              } else {
                _this.handleSubmitFail(_context.t0);
              }

            case 13:
              _context.prev = 13;

              _this.form.find("input[type=submit] ").prop("disabled", false);
              return _context.finish(13);

            case 16:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this, [[0, 10, 13, 16]]);
    }));

    this.handleSubmitFail = function (err) {
      if (typeof _this.onSubmitFail === 'function') {
        _this.onSubmitFail(err, _this);
      } else {
        throw err;
      }
      _this.form.find("input[type=submit] ").removeAttr("disabled");
    };

    var form = params.form,
        rules = params.rules,
        submit = params.submit,
        onSubmitSuccess = params.onSubmitSuccess,
        onSubmitFail = params.onSubmitFail,
        serializeValues = params.serializeValues,
        reset = params.reset,
        _params$validateOptio = params.validateOptions,
        validateOptions = _params$validateOptio === undefined ? {} : _params$validateOptio;

    this.submitHandler = submit;
    form.submit(function (event) {
      return event.preventDefault();
    }).validate((0, _extends3.default)({
      rules: rules,
      onfocusout: function onfocusout(element) {
        this.element(element);
      },
      submitHandler: this.submit
    }, validateOptions));
    this.form = form;
    this.onSubmitSuccess = onSubmitSuccess;
    this.onSubmitFail = onSubmitFail;
    this.serializeValues = serializeValues || defaultSerializeValues;
    this.resetOnSubmit = reset || false;

    // Disable native form validation
    this.form.prop('novalidate', true);
  }

  (0, _createClass3.default)(Form, [{
    key: 'getElement',
    value: function getElement() {
      return this.form;
    }
  }, {
    key: 'showSubmissionErrors',


    /**
     * 
     * @param {*} errors 
     */
    value: function showSubmissionErrors(submissionError) {
      // Filter first error message for each key
      var _errors = (0, _reduce3.default)(submissionError.errors, function (result, messages, key) {
        return (0, _assign2.default)(result, (0, _defineProperty3.default)({}, key, messages[0]));
      }, {});

      this.form.validate().showErrors(_errors);
    }
  }, {
    key: 'populate',
    value: function populate(values) {
      var _this2 = this;

      var mappedValues = (0, _flattenMapToInputValues2.default)(values);

      (0, _forEach2.default)(mappedValues, function (value, key) {
        var $target = _this2.getElement().find(':input[name="' + key + '"]');

        if ($target.length === 0) {
          return;
        }

        switch ($target.attr('type')) {
          case 'radio':
          case 'checkbox':
            $target.filter(function (key, element) {
              var $element = (0, _jquery2.default)(element);
              var checked = $element.val() !== '' && $element.val().toString() == value;

              $element.prop('checked', checked);
            });
            break;

          default:
            $target.val(value);
        }
      });

      return this;
    }
  }, {
    key: 'reset',
    value: function reset() {
      this.form[0].reset();

      return this;
    }
  }]);
  return Form;
}();

var handleApiResponse = exports.handleApiResponse = function handleApiResponse(callback) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  return function () {
    var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(values, form) {
      var response, hasData, error;
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return callback(values, form);

            case 2:
              response = _context2.sent;
              hasData = (0, _typeof3.default)(response.data) === 'object' && response.data;

              // Ensure "success" is true, in addition with "ok" flag

              if (!(response.ok && hasData && response.data.success)) {
                _context2.next = 6;
                break;
              }

              return _context2.abrupt('return', response);

            case 6:
              if (!(hasData && (0, _typeof3.default)(response.data.errors) === 'object' && response.status === 422)) {
                _context2.next = 8;
                break;
              }

              throw new SubmissionError(response.data.errors);

            case 8:
              if (!(hasData && response.data.message)) {
                _context2.next = 10;
                break;
              }

              throw new SubmissionError({ _error: response.data.message });

            case 10:

              // Throw a generic error
              error = new SubmissionError({ _error: 'Ooops! Something went wrong' });
              // Add the response to the submission error

              error.response = response;

              throw error;

            case 13:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined);
    }));

    return function (_x2, _x3) {
      return _ref2.apply(this, arguments);
    };
  }();
};

(0, _addJqueryValidatorMethods2.default)(_jquery2.default);

var spinner = exports.spinner = (0, _jquery2.default)("<i class='fa fa-circle-o-notch fa-spin'></i> Loading");

var enable = exports.enable = function enable() {
  return function (form) {
    form.getElement().find(':submit').each(function () {
      var $button = (0, _jquery2.default)(this);
      $button.prop('disabled', false);

      // Revert to old button content
      var originalContent = $button.data('original-innerHtml');

      if (originalContent) {
        $button.empty().html(originalContent);
      }
    });
  };
};

var disable = exports.disable = function disable() {
  var $loader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : spinner;
  return function (form) {
    form.getElement().find(':submit').each(function () {
      var $button = (0, _jquery2.default)(this);
      $button.prop('disabled', true);

      if ($loader) {
        var originalContent = $button.html();
        $button.data('original-innerHtml', originalContent);
        $button.empty().append($loader);
      }
    });
  };
};

exports.default = Form;