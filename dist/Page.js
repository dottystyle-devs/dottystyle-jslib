"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Page = function () {
  function Page(app) {
    (0, _classCallCheck3.default)(this, Page);

    this.app = app;
  }

  (0, _createClass3.default)(Page, [{
    key: "init",
    value: function init() {}
  }]);
  return Page;
}();

exports.default = Page;