'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Page = exports.Form = undefined;

var _app = require('./app');

var _app2 = _interopRequireDefault(_app);

var _Page = require('./Page');

var _Page2 = _interopRequireDefault(_Page);

var _Form = require('./Form');

var _Form2 = _interopRequireDefault(_Form);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _app2.default;
exports.Form = _Form2.default;
exports.Page = _Page2.default;