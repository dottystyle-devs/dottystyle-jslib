'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var onCreateInstanceDefault = function onCreateInstanceDefault(instance) {
  return instance;
};

/**
  * Create a class loader using dynamic imports in a map/object.
  * 
  * @param {Object} importers
  * @param {Function} [onCreateInstance]
  * @return {Function}
  */
var createDynamicImportMapLoader = function createDynamicImportMapLoader(importers) {
  var onCreateInstance = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : onCreateInstanceDefault;

  return function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(name) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      var Module, instance;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(typeof importers[name] !== 'undefined')) {
                _context.next = 6;
                break;
              }

              _context.next = 3;
              return importers[name]();

            case 3:
              Module = _context.sent;
              instance = new (Function.prototype.bind.apply(Module.default, [null].concat(args)))();
              return _context.abrupt('return', onCreateInstance.apply(undefined, [instance, name].concat(args)));

            case 6:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    return function (_x2) {
      return _ref.apply(this, arguments);
    };
  }();
};

exports.default = createDynamicImportMapLoader;