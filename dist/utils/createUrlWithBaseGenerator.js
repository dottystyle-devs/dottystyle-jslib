'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _normalizeUrl = require('normalize-url');

var _normalizeUrl2 = _interopRequireDefault(_normalizeUrl);

var _trimStart = require('lodash/trimStart');

var _trimStart2 = _interopRequireDefault(_trimStart);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 
 * @param {Function} callback
 * @return {Function}  
 */
var createCallbackGenerator = function createCallbackGenerator(callback) {
  var baseUrl = void 0;

  return function () {
    var path = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

    baseUrl || (baseUrl = (0, _normalizeUrl2.default)(callback()));

    return getPath(baseUrl, path);
  };
};

/**
 * Generate a url given the base url and path
 * 
 * @param {String} baseUrl
 * @param {String} path
 * @return {String} 
 */
var getPath = function getPath(baseUrl) {
  var path = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

  // Remove leading slash
  path = (0, _trimStart2.default)(path, '/');

  return path.length > 0 ? baseUrl + '/' + path : baseUrl;
};

/**
 * 
 * @param {String|Function} baseUrl
 * @return {Function} 
 */
var createUrlWithBaseGenerator = function createUrlWithBaseGenerator(baseUrl) {
  if (typeof baseUrl === 'function') {
    return createCallbackGenerator(baseUrl);
  }

  var normalizedBaseUrl = (0, _normalizeUrl2.default)(baseUrl);

  return function () {
    var path = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return getPath(normalizedBaseUrl, path);
  };
};

exports.default = createUrlWithBaseGenerator;