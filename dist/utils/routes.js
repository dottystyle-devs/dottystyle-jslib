'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _routeParser = require('route-parser');

var _routeParser2 = _interopRequireDefault(_routeParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 
 * @return {String}
 */
var route = function route(pattern) {
  var routeParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  var _route = new _routeParser2.default(pattern);

  return _route.reverse(routeParams);
};

exports.default = route;