'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _forEach = require('lodash/forEach');

var _forEach2 = _interopRequireDefault(_forEach);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var flattenMapToInputValues = function flattenMapToInputValues(value) {
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var result = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  if (value instanceof Array || (typeof value === 'undefined' ? 'undefined' : (0, _typeof3.default)(value)) === 'object' && value && (0, _keys2.default)(value).length > 0) {
    (0, _forEach2.default)(value, function (subValue, subKey) {
      return flattenMapToInputValues(subValue, key ? key + '[' + subKey + ']' : subKey, result);
    });
  } else if (key) {
    result[key] = value;
  }

  return result;
};

exports.default = flattenMapToInputValues;