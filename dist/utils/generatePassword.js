'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = generateRandomString;

var _randomstring = require('randomstring');

var _randomstring2 = _interopRequireDefault(_randomstring);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function generateRandomString() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  return _randomstring2.default.generate(options);
}