"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function ($) {

  $.validator.addMethod("email", function (value, element) {
    return this.optional(element) || /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
  }, 'Please enter a valid email address.');

  $.validator.addMethod("before_date", function (value, element, params) {
    var id = params.id,
        format = params.format;

    var params_val = $(id).val();
    var d1 = (0, _moment2.default)(value, format).toDate();
    var d2 = (0, _moment2.default)(params_val, format).toDate();

    return this.optional(element) || new Date(d1) <= new Date(d2);
  }, 'Must be less than end date.');

  $.validator.addMethod("after_date", function (value, element, params) {
    var id = params.id,
        format = params.format;

    var params_val = $(id).val();
    var d1 = (0, _moment2.default)(value, format).toDate();
    var d2 = (0, _moment2.default)(params_val, format).toDate();

    return this.optional(element) || new Date(d1) >= new Date(d2);
  }, 'Must be greater than start date.');

  $.validator.addMethod("valid_date", function (value, element, params) {
    return this.optional(element) || (0, _moment2.default)(value, params.format).isValid();
  }, function (params, element) {
    return params.message || "Please enter a valid date format.";
  });

  // Add pattern validator, copied from the original source code
  $.validator.addMethod('pattern', function (value, element, param) {
    if (this.optional(element)) {
      return true;
    }

    if (typeof param === "string") {
      param = new RegExp("^(?:" + param + ")$");
    }

    return param.test(value);
  }, 'Invalid format');
};

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }