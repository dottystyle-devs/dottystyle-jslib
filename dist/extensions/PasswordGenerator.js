'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _randomstring = require('randomstring');

var _randomstring2 = _interopRequireDefault(_randomstring);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var generateRandomString = function generateRandomString() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return _randomstring2.default.generate(options);
};

var PasswordGenerator = function () {
  function PasswordGenerator($element, options) {
    var _this = this;

    (0, _classCallCheck3.default)(this, PasswordGenerator);

    this.$element = $element;
    this.randomOptions = options;

    this.$element.on('click', '.generate', function (evt) {
      _this.generate();
      evt.preventDefault();
    });
  }

  (0, _createClass3.default)(PasswordGenerator, [{
    key: 'generate',
    value: function generate() {
      var $password = this.$element.find(':input.password');
      var password = generateRandomString(this.randomOptions);
      $password.val(password);
      this.$element.trigger('password:generated', [password, $password]);
    }
  }]);
  return PasswordGenerator;
}();

var PasswordGeneratorExtension = function () {
  function PasswordGeneratorExtension(options) {
    (0, _classCallCheck3.default)(this, PasswordGeneratorExtension);

    _initialiseProps.call(this);

    this.options = (0, _extends3.default)({
      length: 12
    }, options);
  }

  (0, _createClass3.default)(PasswordGeneratorExtension, [{
    key: 'name',
    value: function name() {
      return 'Password Generator';
    }
  }, {
    key: 'install',
    value: function install(app) {
      app.passwordGenerator = this.createPasswordGenerator;
    }
  }]);
  return PasswordGeneratorExtension;
}();

var _initialiseProps = function _initialiseProps() {
  var _this2 = this;

  this.createPasswordGenerator = function (selector) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    return new PasswordGenerator((0, _jquery2.default)(selector), (0, _extends3.default)({}, _this2.options, options));
  };
};

exports.default = PasswordGeneratorExtension;