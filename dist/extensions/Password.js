'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PasswordWithToggle = function () {
  function PasswordWithToggle($element, options) {
    (0, _classCallCheck3.default)(this, PasswordWithToggle);

    this.$element = $element;
    this.masked = true;
    this.maskedClass = options.maskedClass;
    this.unmaskedClass = options.unmaskedClass;

    this.$element.on('click', '.toggle-mask', this.click.bind(this));

    this.mask();
  }

  (0, _createClass3.default)(PasswordWithToggle, [{
    key: 'click',
    value: function click(evt) {
      this.masked ? this.unmask() : this.mask();
      evt.preventDefault();
    }
  }, {
    key: 'passwordElement',
    value: function passwordElement() {
      return this.$element.find(':input.password');
    }
  }, {
    key: 'toggleElement',
    value: function toggleElement() {
      return this.$element.find('.toggle-mask');
    }
  }, {
    key: 'mask',
    value: function mask() {
      this.passwordElement().attr('type', 'password');
      this.toggleElement().removeClass(this.unmaskedClass).addClass(this.maskedClass);
      this.masked = true;
    }
  }, {
    key: 'unmask',
    value: function unmask() {
      this.passwordElement().attr('type', 'text');
      this.toggleElement().removeClass(this.maskedClass).addClass(this.unmaskedClass);
      this.masked = false;
    }
  }]);
  return PasswordWithToggle;
}();

var Password = function () {
  function Password() {
    (0, _classCallCheck3.default)(this, Password);

    this.createPassword = function (selector, options) {
      return new PasswordWithToggle((0, _jquery2.default)(selector).closest('.password-wrapper'), options);
    };
  }

  (0, _createClass3.default)(Password, [{
    key: 'name',
    value: function name() {
      return 'Password';
    }
  }, {
    key: 'install',
    value: function install(app) {
      app.password = this.createPassword;
    }
  }]);
  return Password;
}();

exports.default = Password;