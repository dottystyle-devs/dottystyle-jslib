'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BootstrapTabs = undefined;

var _BootstrapTabs = require('./BootstrapTabs');

var _BootstrapTabs2 = _interopRequireDefault(_BootstrapTabs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.BootstrapTabs = _BootstrapTabs2.default;