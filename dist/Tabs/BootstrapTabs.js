'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _eventEmitter = require('event-emitter');

var _eventEmitter2 = _interopRequireDefault(_eventEmitter);

var _trimStart = require('lodash/trimStart');

var _trimStart2 = _interopRequireDefault(_trimStart);

var _pick = require('lodash/pick');

var _pick2 = _interopRequireDefault(_pick);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BootstrapTabs = function () {
  function BootstrapTabs(target) {
    var _this = this;

    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck3.default)(this, BootstrapTabs);

    this.$element = (0, _jquery2.default)(target);
    options = (0, _pick2.default)(options, ['bindLocation']);

    this.activeTabKey = null;

    this.options = (0, _extends3.default)({
      bindLocation: true
    }, options);

    var events = ['show', 'shown', 'hide', 'hidden'];

    events.forEach(function (eventName) {
      _this.attachCustomTabsEventListener(eventName);
    });

    if (this.options.bindLocation) {
      this.bindLocationListener();
    }
  }

  (0, _createClass3.default)(BootstrapTabs, [{
    key: 'bindLocationListener',
    value: function bindLocationListener() {
      var _this2 = this;

      (0, _jquery2.default)(window).on('hashchange', function () {
        _this2.showFromLocation();
      });

      this.on('show', function (_ref) {
        var tab = _ref.tab;

        window.location.hash = tab.attr('href');
      });
    }
  }, {
    key: 'attachCustomTabsEventListener',
    value: function attachCustomTabsEventListener(eventName) {
      var _this3 = this;

      this.$element.on(eventName + '.bs.tab', 'a[role=tab]', function (evt) {
        var $tab = (0, _jquery2.default)(evt.target);
        // Use key attribute/data if present on the tab element
        var tabId = _this3.getTabKey($tab);
        var $pane = (0, _jquery2.default)($tab.attr('href'));
        var customEvent = {
          tabId: tabId,
          tab: $tab,
          pane: $pane,
          originalEvent: evt,
          tabKey: tabId,
          preventDefault: function preventDefault() {
            return evt.preventDefault();
          }
        };

        _this3.emit(eventName, customEvent);
        _this3.emit(eventName + '.' + tabId, customEvent);
      });
    }
  }, {
    key: 'getElement',
    value: function getElement() {
      return this.$element;
    }
  }, {
    key: 'getNav',
    value: function getNav() {
      return this.$element.find('[role=tablist],.nav-tabs');
    }
  }, {
    key: 'show',
    value: function show(tabKey) {
      var $tab = void 0;

      if ($tab = this.getTabNav(tabKey)) {
        $tab.tab('show');
      }
    }
  }, {
    key: 'getTabKey',
    value: function getTabKey($tab) {
      return $tab.data('tab-key') || (0, _trimStart2.default)($tab.attr('href'), '#');
    }
  }, {
    key: 'getTabNav',
    value: function getTabNav(tabKey) {
      var self = this;

      return this.getTabNavs().filter(function () {
        var $tab = (0, _jquery2.default)(this);

        if (self.getTabKey($tab) === tabKey) {
          return true;
        }
      }).first();
    }
  }, {
    key: 'getTabNavs',
    value: function getTabNavs() {
      return this.getNav().find('a[role=tab]');
    }
  }, {
    key: 'getTabPane',
    value: function getTabPane(tabKey) {
      return (0, _jquery2.default)(this.getTabNav(tabKey).attr('href'));
    }
  }, {
    key: 'showFromLocation',
    value: function showFromLocation() {
      var hash = window.location.hash;

      var self = this;
      var tabKeyShown = false;

      if (hash && hash.length > 0) {
        this.getTabNavs().each(function () {
          if ((0, _jquery2.default)(this).attr('href') !== hash) {
            return;
          }

          self.show(tabKeyShown = self.getTabKey((0, _jquery2.default)(this)));

          return false;
        });
      }

      return tabKeyShown;
    }
  }]);
  return BootstrapTabs;
}();

(0, _eventEmitter2.default)(BootstrapTabs.prototype);

exports.default = BootstrapTabs;