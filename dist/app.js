'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _eventEmitter = require('event-emitter');

var _eventEmitter2 = _interopRequireDefault(_eventEmitter);

var _hasListeners = require('event-emitter/has-listeners');

var _hasListeners2 = _interopRequireDefault(_hasListeners);

var _forEach = require('lodash/forEach');

var _forEach2 = _interopRequireDefault(_forEach);

var _pick2 = require('lodash/pick');

var _pick3 = _interopRequireDefault(_pick2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var App = function () {
  function App(_ref) {
    var _this = this;

    var loadPage = _ref.loadPage,
        loadModule = _ref.loadModule,
        methods = _ref.methods;
    (0, _classCallCheck3.default)(this, App);

    this.handleDocumentLoad = function () {
      _this.triggerLoad(_this);
    };

    this.modules = {};
    this.loadPromise = new _promise2.default(function (resolve) {
      return _this.triggerLoad = resolve;
    });
    this._loadPage = loadPage;
    this._loadModule = loadModule;
    this.events = (0, _eventEmitter2.default)();

    (0, _jquery2.default)(this.handleDocumentLoad);
  }

  /**
   * Trigger the load event of the application, effectively calling all load callback attached. 
   * 
   * @return {void}
   */


  (0, _createClass3.default)(App, [{
    key: 'getModule',


    /**
     * Get a module on the application
     * 
     * @param {String} name
     * @param {Object} [options]
     * @return {Promise}
     */
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(name) {
        var module;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (this.isModuleLoaded(name)) {
                  _context.next = 14;
                  break;
                }

                _context.prev = 1;
                _context.next = 4;
                return this._loadModule(name, this);

              case 4:
                module = _context.sent;

                if (module) {
                  _context.next = 7;
                  break;
                }

                throw "Invalid module returned";

              case 7:

                this.modules[name] = module;
                _context.next = 14;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context['catch'](1);

                this.events.emit('moduleLoadError', _context.t0);
                throw _context.t0;

              case 14:
                return _context.abrupt('return', this.modules[name]);

              case 15:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 10]]);
      }));

      function getModule(_x) {
        return _ref2.apply(this, arguments);
      }

      return getModule;
    }()

    /**
     * Determine whether module has been loaded or not.
     * 
     * @param {String} name 
     * @return {Boolean}
     */

  }, {
    key: 'isModuleLoaded',
    value: function isModuleLoaded(name) {
      return typeof this.modules[name] !== 'undefined';
    }

    /**
     * Load a page
     * 
     * @param {String} name
     * @param {*} [options]
     * @return {Promise}
     */

  }, {
    key: 'loadPage',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(name) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
        var page;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return this._loadPage(name, this, options);

              case 3:
                page = _context2.sent;

                if (page) {
                  _context2.next = 6;
                  break;
                }

                throw "Invalid page";

              case 6:

                this.events.emit('pageLoaded', page, options, this);
                this.events.emit('pageLoaded.' + name, page, options, this);

                return _context2.abrupt('return', page);

              case 11:
                _context2.prev = 11;
                _context2.t0 = _context2['catch'](0);

                this.events.emit('pageLoadError', name);
                throw _context2.t0;

              case 15:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 11]]);
      }));

      function loadPage(_x2) {
        return _ref3.apply(this, arguments);
      }

      return loadPage;
    }()

    /**
     * Attach a callback that will be fired once application is ready.
     * 
     * @param {Function} callback
     * @return {void} 
     */

  }, {
    key: 'ready',
    value: function ready(callback) {
      this.loadPromise.then(callback);
    }

    /**
     * Add an callback when application has been loaded.
     * 
     * @param {Function} callback
     * @return {void}
     */

  }, {
    key: 'on',
    value: function on(event, callback) {
      if (event === 'load') {
        this.loadPromise.then(callback);
      } else {
        this.events.on(event, callback);
      }
    }

    /**
     * Remove listener for an event.
     * 
     * @param {String} event
     * @param {Function} callback
     * @return {void}
     */

  }, {
    key: 'off',
    value: function off(event, callback) {
      this.events.off(event, callback);
    }

    /**
     * 
     * @param {String} event
     * @param {...*} args
     * @return {void}
     */

  }, {
    key: 'trigger',
    value: function trigger(event) {
      if ((0, _hasListeners2.default)(this.events, event)) {
        var _events;

        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        (_events = this.events).emit.apply(_events, [event].concat(args));
      }
    }
  }]);
  return App;
}();

var createApp = function createApp(options) {
  var app = new App((0, _pick3.default)(options, ['loadPage', 'loadModule']));

  if (options.extensions) {
    options.extensions.forEach(function (extension) {
      return extension.install(app);
    });
  }

  return app;
};

exports.default = createApp;